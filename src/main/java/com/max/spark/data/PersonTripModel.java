package com.max.spark.data;

import lombok.Data;

@Data
public class PersonTripModel {

    private Integer abroadTripsCount;

    private String ageCategory;

    private String passportNumber;

    private String monthNumber;
}
