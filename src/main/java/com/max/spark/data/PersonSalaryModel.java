package com.max.spark.data;

import lombok.Data;

@Data
public class PersonSalaryModel {

    private Integer salary;

    private String ageCategory;

    private String passportNumber;

    private String monthNumber;
}
