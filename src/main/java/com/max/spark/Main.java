package com.max.spark;


import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    /**
     * Главный метод программы
     * @param args аргументы программы
     *             args[0] - мод запуска программы: {generage, processing}
     *             args[1] - имя пользователя для используемой схемы БД
     *             args[2] - пароль пользователя для используемой схемы БД
     *             args[3] - путь до генерируемых файлов
     */
    public static void main(String[] args) {

        String user = "spark";
        String password = "spark";

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (!args[1].isEmpty() && !args[2].isEmpty()) {
            user = args[1];
            password = args[2];
        } else {
            throw new RuntimeException("You should pass 3 arguments:\n1) mod={generate, processin}\n2) data_base_username\n3) data_base_password");
        }

        if (args[0].equals("generate")) {

            generate(user, password, args[3]);
        } else if (args[0].equals("processing")) {
            processing();
        } else {
            throw new RuntimeException("Unknown execution mod exception. There are only two mods:\n1) generate\n 2) processing.");
        }
    }

    /**
     *  Метод обработки сгенеренных файлов и
     *  подсчета средних значений
     */
    private static void processing() {

        SparkConf conf = new SparkConf()
                .setAppName("Spark Big Data Processing")
                .setMaster("local");

        SparkSession spark = SparkSession
                .builder()
                .config(conf)
                .getOrCreate();

        Configuration hadoopConf = new Configuration();
        hadoopConf.addResource(new Path("core-site.xml"));
        hadoopConf.addResource(new Path("hdfs-site.xml"));

        hadoopConf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );
        hadoopConf.set("fs.file.impl",
                org.apache.hadoop.fs.LocalFileSystem.class.getName()
        );

        String pathToSalaryFile = "/opt/spark/salary.csv";
        String pathToTripsFile = "/opt/spark/trips.csv";
        readFromHdfs(hadoopConf, pathToSalaryFile, "age,monthNumber,passportNumber,salary\n");
        readFromHdfs(hadoopConf, pathToTripsFile, "abroadTripsCount,age,monthNumber,passportNumber\n");

        Dataset<Row> salaryDataSet = spark.read().format("com.databricks.spark.csv").option("header", "true").load(pathToSalaryFile);
        Dataset<Row> tripsDataSet = spark.read().format("com.databricks.spark.csv").option("header", "true").load(pathToTripsFile);

        spark.udf().register("myAverage", new MyAverage());

        salaryDataSet.createOrReplaceTempView("salary");
        tripsDataSet.createOrReplaceTempView("trips");

        Dataset<Row> avgSalary = spark.sql("SELECT myAverage(salary) as average_salary, age FROM salary GROUP BY age");
        Dataset<Row> avgTrips = spark.sql("SELECT myAverage(abroadTripsCount) as average_trips, age FROM trips GROUP BY age");

        Dataset<Row> avgJoin = avgSalary.join(avgTrips,"age");
        avgJoin.show();
    }


    /**
     * Метод генерации входных файлов
     * Генерируются два файла inputSalary.json и inputTrips.json
     * Далее для каждого файла с помощью sparkSQL данные записываются в таблицы в базы БД
     *
     * @param user имя пользователя к схеме БД
     * @param password пароль пользователя к схеме БД
     * @param pathToGeneratedFiles путь к директории для сгенерированных файлов
     */
    private static void generate(String user, String password, String pathToGeneratedFiles) {
        InputSalaryFileGenerator generator = new InputSalaryFileGenerator();
        generator.generateInputFiles(2, pathToGeneratedFiles);

        SparkConf conf = new SparkConf()
                .setAppName("Spark Big Data Processing")
                .setMaster("local");

        SparkSession spark = SparkSession
                .builder()
                .config(conf)
                .getOrCreate();

        Dataset<Row> salaryDataFrame = spark.read().json(pathToGeneratedFiles + "/inputSalary.json");
        Dataset<Row> tripsDataFrame = spark.read().json(pathToGeneratedFiles + "/inputTrips.json");


        salaryDataFrame.write()
                .format("jdbc")
                .option("url", "jdbc:mysql://localhost:3306/spark?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC")
                .option("dbtable", "spark.salary")
                .option("user", user)
                .option("password", password)
                .mode(SaveMode.Overwrite)
                .saveAsTable("salary");

        tripsDataFrame.write()
                .format("jdbc")
                .option("url", "jdbc:mysql://localhost:3306/spark?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC")
                .option("dbtable", "spark.trips")
                .option("user", user)
                .option("password", password)
                .mode(SaveMode.Overwrite)
                .saveAsTable("trips");
    }

    /**
     * Метод для считывания файлов из HDFS
     *
     * @param hadoopConf конфигурация для hadoop
     * @param generatedFilePath путь к файлам, которые в которые будут записаны данные из файлов в системе hdfs
     * @param headers заголовоки для создаваемых файлов. (просто строка, которая записывается первой в файле). Для того, чтобы
     *                спарк при парсинге данных файлов мог определить названия колонок
     */
    private static void readFromHdfs(Configuration hadoopConf, String generatedFilePath, String headers) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the file path...");
        try {
            String filePath = br.readLine();

            Path path = new Path("hdfs://quickstart.cloudera:8020" + filePath);
            FileSystem fs = path.getFileSystem(hadoopConf);
            FSDataInputStream inputStream = fs.open(path);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            File file = new File(generatedFilePath);
            file.createNewFile();
            OutputStream outputStream = new FileOutputStream(file);
            IOUtils.copy(reader, outputStream);
            reader.close();
            outputStream.close();
            fs.close();

            byte[] encoded = Files.readAllBytes(Paths.get(generatedFilePath));
            String string = headers + new String(encoded);

            file.delete();

            BufferedWriter writer = new BufferedWriter(new FileWriter(generatedFilePath));
            writer.write(string);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error while processing the file path", e);
        }
    }
}
