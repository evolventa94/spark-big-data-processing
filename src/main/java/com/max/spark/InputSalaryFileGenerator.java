package com.max.spark;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.max.spark.data.Month;
import com.max.spark.data.PersonSalaryModel;
import com.max.spark.data.PersonTripModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class InputSalaryFileGenerator {

    private int minSalary = 50000;

    private int maxSalary = 5000000;

    private ObjectMapper objectMapper = new ObjectMapper();

    private Random random = new Random();

    List<PersonSalaryModel> personSalaryModelList = new ArrayList<>();
    List<PersonTripModel> personTripModelList = new ArrayList<>();

    /**
     *
     * Метод генерации входных файлов. Тип генерируемых файлов .json
     * @param count - количество граждан, информация о которых будет помещена в файл
     * @param generateFilesPath - директория для генерируемых файлов
     *
     */
    public void generateInputFiles(int count, String generateFilesPath) {
        PersonSalaryModel salaryModel;
        PersonTripModel tripModel;

        for (int i = 0; i < count; i++) {
            Integer passportNumber = 111111 + random.nextInt(888888);
            Integer age = 20 + random.nextInt(60);
            String ageCategory = defineAgeCategory(age);
            for (Month month: Month.values()) {
                salaryModel = new PersonSalaryModel();
                salaryModel.setPassportNumber(String.valueOf(passportNumber));
                salaryModel.setSalary(minSalary + random.nextInt(maxSalary - minSalary));
                salaryModel.setMonthNumber(String.valueOf(month.getNumber()));
                salaryModel.setAgeCategory(ageCategory);

                tripModel = new PersonTripModel();
                tripModel.setPassportNumber(String.valueOf(passportNumber));
                tripModel.setMonthNumber(String.valueOf(month.getNumber()));
                tripModel.setAbroadTripsCount(1 + random.nextInt(4));
                tripModel.setAgeCategory(ageCategory);

                personTripModelList.add(tripModel);
                personSalaryModelList.add(salaryModel);
            }
        }

        try {
            objectMapper.writeValue(new File(generateFilesPath + "/inputSalary.json"), personSalaryModelList);
            objectMapper.writeValue(new File(generateFilesPath + "/inputTrips.json"), personTripModelList);
        } catch (IOException e) {
            throw new RuntimeException("Error while generating file");
        }
    }

    /**
     * Метод определяющий возрастрную катерию по возрасту
     * @param age - возраст
     * @return возрастная категория: {young, middle, old}
     */
    private String defineAgeCategory(Integer age) {
        if(age >= 20 && age < 40) {
            return "young";
        } else if(age >= 40 && age < 60) {
            return "middle";
        } else if(age >= 60 && age <= 80) {
            return "old";
        } else {
            throw new RuntimeException("Error while defining age category");
        }
    }


}
