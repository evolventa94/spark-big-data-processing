create table salary
(
	id int auto_increment
		primary key,
	monthNumber varchar(255) not null,
	passportNumber varchar(255) null,
	salary int null,
	ageCategory VARCHAR(50) null,
	constraint `person.salary_id_uindex`
	unique (id)
)
	engine=InnoDB
;

create table trips
(
	id int auto_increment
		primary key,
	passportNumber varchar(255) null,
	monthNumber varchar(2) null,
	abroadTripsCount int null,
	ageCategory VARCHAR(50) null,
	constraint `person.trips_id_uindex`
	unique (id)
)
	engine=InnoDB
;