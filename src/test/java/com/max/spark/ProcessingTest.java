package com.max.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;

public class ProcessingTest {

    @Test
    public void processingTest() {

        SparkConf conf = new SparkConf()
                .setAppName("Spark Big Data Processing")
                .setMaster("local");

        SparkSession spark = SparkSession
                .builder()
                .config(conf)
                .getOrCreate();

        String pathToSalaryFile = "src/test/resources/salary.csv";
        String pathToTripsFile = "src/test/resources/trips.csv";

        Dataset<Row> salaryDataSet = spark.read().format("com.databricks.spark.csv").option("header", "true").load(pathToSalaryFile);
        Dataset<Row> tripsDataSet = spark.read().format("com.databricks.spark.csv").option("header", "true").load(pathToTripsFile);

        spark.udf().register("myAverage", new MyAverage());

        salaryDataSet.createOrReplaceTempView("salary");
        tripsDataSet.createOrReplaceTempView("trips");

        Dataset<Row> avgSalary = spark.sql("SELECT myAverage(salary) as average_salary, age FROM salary GROUP BY age");
        Dataset<Row> avgTrips = spark.sql("SELECT myAverage(abroadTripsCount) as average_trips, age FROM trips GROUP BY age");

        Dataset<Row> avgJoin = avgSalary.join(avgTrips,"age");
        avgJoin.show();
    }

}